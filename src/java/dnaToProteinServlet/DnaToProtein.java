/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnaToProteinServlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Willem
 */
public class DnaToProtein extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String outputString = "";
        DnaToProtein instance = new DnaToProtein();
        DnaSequence dnaSequence = new DnaSequence(request.getParameter("dna").toString());

//
//        set dnaSequence translations
        dnaSequence.setFrame();
        dnaSequence.setFrameReverse();
        outputString = instance.filterOutput(request, dnaSequence);
        request.setAttribute("results", outputString);

        RequestDispatcher view = request.getRequestDispatcher("translateResults.jsp");
        view.forward(request, response);

    }

    public String filterOutput(HttpServletRequest request, DnaSequence dnaSequence) {
        String outputString = "";
        String seq1 = request.getParameter("meth1");
        String seq2 = request.getParameter("meth2");
        String seq3 = request.getParameter("meth3");

        String seq4 = request.getParameter("meth_1");
        String seq5 = request.getParameter("meth_2");
        String seq6 = request.getParameter("meth_3");
        if (seq1 != null) {
            outputString += "<br>frame +1 <br>" + dnaSequence.getFrame1() + "<br>";
        }
        if (seq2 != null) {
            outputString += "<br>frame +2 <br>" + dnaSequence.getFrame2() + "<br>";
        }
        if (seq3 != null) {
            outputString += "<br>frame +3 <br>" + dnaSequence.getFrame3() + "<br>";
        }
        if (seq4 != null) {
            outputString += "<br>frame -1 <br>" + dnaSequence.getFrame1Reverse() + "<br>";
        }
        if (seq5 != null) {
            outputString += "<br>frame -2 <br>" + dnaSequence.getFrame2Reverse() + "<br>";
        }
        if (seq6 != null) {
            outputString += "<br>frame -3 <br>" + dnaSequence.getFrame3Reverse() + "<br>";
        }
        return outputString;

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
