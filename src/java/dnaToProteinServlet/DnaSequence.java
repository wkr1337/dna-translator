/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dnaToProteinServlet;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Willem
 */
public class DnaSequence {

    private String dnaSequene;
    private String reverseComplement;
    private String frame1;
    private String frame2;
    private String frame3;
    private String frame1Reverse;
    private String frame2Reverse;
    private String frame3Reverse;
    ArrayList<String> allFrames = new ArrayList<String>();

    public ArrayList<String> getAllFrames() {
        if (allFrames.isEmpty()) {
            setAllFrames();
        }
        return allFrames;
    }

    /**
     * Set all frames
     */
    public void setAllFrames() {
        this.setFrame();
        this.setFrameReverse();
        this.allFrames.add(frame1);
        this.allFrames.add(frame2);
        this.allFrames.add(frame3);
        this.allFrames.add(frame1Reverse);
        this.allFrames.add(frame2Reverse);
        this.allFrames.add(frame3Reverse);
    }

    /**
     * this is a dnaSequence data wrapper
     */
    public DnaSequence(String dnaSequence1) {
        setDnaSequene(dnaSequence1);
        setReverseComplement();

    }

    public void setFrame() {
        ArrayList<String> proteinList = translate_dna(this.dnaSequene);

        this.frame1 = proteinList.get(0);
        this.frame2 = proteinList.get(1);
        this.frame3 = proteinList.get(2);

    }

    public void setFrameReverse() {
        ArrayList<String> proteinList = translate_dna(this.reverseComplement);

        this.frame1Reverse = proteinList.get(0);
        this.frame2Reverse = proteinList.get(1);
        this.frame3Reverse = proteinList.get(2);
    }

    public String getFrame1() {
        return frame1;
    }

    public String getFrame2() {
        return frame2;
    }

    public String getFrame3() {
        return frame3;
    }

    public String getFrame1Reverse() {
        return frame1Reverse;
    }

    public String getFrame2Reverse() {
        return frame2Reverse;
    }

    public String getFrame3Reverse() {
        return frame3Reverse;
    }

    /**
     * @return HashMap of complement DNA letters
     */
    public HashMap<String, String> makeDnaComplementMap() {
        HashMap<String, String> dnaComplement = new HashMap<String, String>();
        dnaComplement.put("A", "T");
        dnaComplement.put("T", "A");
        dnaComplement.put("C", "G");
        dnaComplement.put("G", "C");
        return dnaComplement;
    }

    public String getDnaSequene() {
        return dnaSequene;
    }

    public void setDnaSequene(String dnaSequene) {
        this.dnaSequene = dnaSequene;
    }

    public String getReverseComplement() {
        return reverseComplement;
    }

    public void setReverseComplement() {
        HashMap<String, String> dnaComplement = makeDnaComplementMap();
        this.reverseComplement = "";
        for (int i = dnaSequene.length(); i > 0; i--) {
            this.reverseComplement += dnaComplement.get(String.valueOf(dnaSequene.charAt(i - 1)));
        }

    }

    /**
     * Translates three dna sequences to protein sequence
     *
     * @return ArrayList of three protein sequences.
     */
    public ArrayList<String> translate_dna(String sequence) {

        HashMap<String, String> codonTable = makeCodonTable();
        ArrayList<String> proteinList = new ArrayList<String>();
        String proteinSequence1 = "";
        String proteinSequence2 = "";
        String proteinSequence3 = "";

        for (int i = 0; i < sequence.length() - 2; i++) {
            String codon = sequence.substring(i, i + 3);
//            translate();
            if (codon.contentEquals("TAA") || codon.contentEquals("TAG") || codon.contentEquals("TGA")) {
            }
            if (i % 3 == 0) {
                proteinSequence1 += (codonTable.get(codon));

            }
            if (i % 3 == 1) {
                proteinSequence2 += (codonTable.get(codon));
            }

            if (i % 3 == 2) {
                proteinSequence3 += (codonTable.get(codon));
            }

        }

        proteinList.add(proteinSequence1);
        proteinList.add(proteinSequence2);
        proteinList.add(proteinSequence3);

        return proteinList;

    }

    public HashMap<String, String> makeCodonTable() {
        HashMap<String, String> codonTable = new HashMap<String, String>();

        codonTable.put("ATA", "I");
        codonTable.put("ATC", "I");
        codonTable.put("ATT", "I");
        codonTable.put("ATG", "M");

        codonTable.put("ACA", "T");
        codonTable.put("ACC", "T");
        codonTable.put("ACG", "T");
        codonTable.put("ACT", "T");

        codonTable.put("AAC", "N");
        codonTable.put("AAT", "N");
        codonTable.put("AAA", "K");
        codonTable.put("AAG", "K");

        codonTable.put("AGC", "S");
        codonTable.put("AGT", "S");
        codonTable.put("AGA", "R");
        codonTable.put("AGG", "R");

        codonTable.put("CTA", "L");
        codonTable.put("CTC", "L");
        codonTable.put("CTG", "L");
        codonTable.put("CTT", "L");

        codonTable.put("CCA", "P");
        codonTable.put("CCC", "P");
        codonTable.put("CCG", "P");
        codonTable.put("CCT", "P");

        codonTable.put("CAC", "H");
        codonTable.put("CAT", "H");
        codonTable.put("CAA", "Q");
        codonTable.put("CAG", "Q");

        codonTable.put("CGA", "R");
        codonTable.put("CGC", "R");
        codonTable.put("CGG", "R");
        codonTable.put("CGT", "R");

        codonTable.put("GTA", "V");
        codonTable.put("GTC", "V");
        codonTable.put("GTG", "V");
        codonTable.put("GTT", "V");

        codonTable.put("GCA", "A");
        codonTable.put("GCC", "A");
        codonTable.put("GCG", "A");
        codonTable.put("GCT", "A");

        codonTable.put("GAC", "D");
        codonTable.put("GAT", "D");
        codonTable.put("GAA", "E");
        codonTable.put("GAG", "E");

        codonTable.put("GGA", "G");
        codonTable.put("GGC", "G");
        codonTable.put("GGG", "G");
        codonTable.put("GGT", "G");

        codonTable.put("TCA", "S");
        codonTable.put("TCC", "S");
        codonTable.put("TCG", "S");
        codonTable.put("TCT", "S");

        codonTable.put("TTC", "F");
        codonTable.put("TTT", "F");
        codonTable.put("TTA", "L");
        codonTable.put("TTG", "L");

        codonTable.put("TAC", "Y");
        codonTable.put("TAT", "Y");
        codonTable.put("TAA", "_");
        codonTable.put("TAG", "_");

        codonTable.put("TGC", "C");
        codonTable.put("TGT", "C");
        codonTable.put("TGA", "_");
        codonTable.put("TGG", "W");
        return codonTable;
    }
}
