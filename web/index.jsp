<%-- 
    Document   : index
    Created on : Nov 24, 2014, 11:00:12 PM
    Author     : Willem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home page</title>
    </head>
    <body>
        <h1>Hello!</h1>
        <h3>Current time is ${requestScope.date}</h3>
        <c:choose>
            <c:when test = "${sessionScope.user != null}">
                <h1>Hello ${sessionScope.user.name}</h1> 
                </c:when>
            <c:otherwise>
                 <jsp:include page="login.jsp" />
                </c:otherwise>
        </c:choose>
                <a href = "dnaTranslator.jsp" >DNA Translator tool</a>
    </body>
</html>
