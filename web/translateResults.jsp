<%-- 
    Document   : translateResults
    Created on : Feb 4, 2017, 4:31:49 PM
    Author     : Willem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RESULTS</title>
    </head>
    <body>
       
        <c:choose>
            <c:when test = "${requestScope.results} != null}">
                
            </c:when>
            <c:otherwise>
                <h3>You are not logged in to our site! Please do so first </h3>
                <jsp:include page="login.jsp" />
            </c:otherwise>
        </c:choose>
        <h3>proteine   <br>
            ${requestScope.results}</h3>

    </body>
</html>
