<%-- 
    Document   : content
    Created on : Jan 31, 2017, 5:20:15 PM
    Author     : Willem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>content</title>
    </head> 
   <body>
            <h1>Secret Content</h1>
            <c:choose>
            <c:when test = "${sessionScope.user != null}">
                <h1>Hello ${sessionScope.user.name}</h1>   
                secret content
            </c:when>
            <c:otherwise>
                <h3>You are not logged in to our site! Please do so first </h3>
                <jsp:include page="login.jsp" />
            </c:otherwise>
        </c:choose>
    </body>
    
</html>
